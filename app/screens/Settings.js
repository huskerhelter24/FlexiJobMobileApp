import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';
import { ListItem } from 'react-native-elements';
import firebase from 'react-native-firebase';

const list = [
  {
    title: 'My Profile',
    icon: 'person',
    path: 'Profile'
  },
  {
    title: 'App Preferences',
    icon: 'compare-arrows',
    path: ''
  },
  {
      title: 'Saved Jobs',
      icon: 'adjust',
      path: ''
  },
  // {
  //     title: 'Logout',
  //     icon: 'block',
  //     path: ''
  // },
]

export default class Settings extends Component {

  state = {errorMessage: null }

  static navigationOptions = {
      title: 'Settings',
      headerStyle: {
        backgroundColor: '#090e25',
      },
      headerTintColor: '#fff',
  };

  signOutUser = () => {
      firebase
        .auth().signOut()
        .then(()=> this.props.navigation.navigate('Login'))
        .catch(error => this.setState({errorMessage: error.message}))
  }

  render() {
    return (
      <View style={styles.container}>
        {/* <Text style={styles.title}>Settings</Text> */}
        {
          list.map((item, i) => (
          <ListItem
              key={i}
              title={item.title}
              leftIcon={{ name: item.icon }}
              rightIcon={{ name: 'arrow-forward'}}
              onPress={() => this.props.navigation.navigate(item.path)}
          />
          ))
        }
        <Button onPress={this.signOutUser} title="Sign Out"/>
        {this.state.errorMessage &&
          <Text style={{ color: 'red' }}>
            {this.state.errorMessage}
          </Text>}
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'flex-start',
    backgroundColor: '#F5FCFF',
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});