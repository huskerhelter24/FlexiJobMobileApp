import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import { SearchBar } from 'react-native-elements';
import { Dropdown } from 'react-native-material-dropdown';

export default class Search extends Component {

    state = {
        search: '',
    };

    updateSearch = search => {
        this.setState({ search });
    };

    render() {
        const { search } = this.state;
        let data = [{
            value: 'jakarta',
        }, {
            value: 'Depok',
        }, {
            value: 'Bogor',
        }, {
            value: 'Tangerang',
        }, {
            value: 'Bekasi',
        }];


        return (
        <View style={styles.container}>
           <SearchBar
                placeholder="Type Here..."
                onChangeText={this.updateSearch}
                value={search}
                lightTheme true
                containerStyle={styles.SearchBar}
            />
            <Dropdown
                label='Location'
                data={data}
                containerStyle={styles.DropdownBar}
            />
        </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-start',
    backgroundColor: '#F5FCFF',
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  SearchBar: {
    width: '100%'
  },
  DropdownBar: {
    width: '100%',
    paddingLeft: 10,
    paddingRight: 10
  }
});