import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  TouchableOpacity
} from 'react-native';
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';

const horizontalMargin = 20;
const slideWidth = 280;
const sliderWidth = Dimensions.get('window').width;
const itemWidth = slideWidth + horizontalMargin * 2;
const itemHeight = 400;

data = [
    {
        title: 'IT Support Staff - Web Developer',
        company: 'PT Walk Brains Indonesia',
        description: 'Responsibilities Developing high-performance, reliable, and clean frontend web. Qualifications 1+ years experience in similar roles.',
        salary: 'Rp. Not Specified',
        location: 'Jakarta Barat (Jakarta Raya)',
        posted: '5 hours ago'

    },
    {
        title: 'Backend Staff - Technician',
        company: 'PT Where Ya at',
        description: ' Completes applications development by coordinating requirements, schedules and activities.',
        salary: 'Rp. 15.000.000',
        location: 'Jakarta Barat (Jakarta Raya)',
        posted: '1 hours ago'
    },
    {
        title: 'Office Boy - Fotokopi dan Menyapu',
        company: 'PT Rich Gang Lifestyle',
        description: 'Membuat proses analisa dan rancangan website/ landing page/mobile platform.',
        salary: 'Rp. Not Specified',
        location: 'Jakarta Barat (Jakarta Raya)',
        posted: '12 hours ago'
    },
    {
        title: 'Backend Developer (Python)',
        company: 'PT Yung RIch Bih',
        description: 'Responsible for writing server-side web application logic in Python and/or variants of it',
        salary: 'Rp. 8.000.000',
        location: 'Jakarta Barat (Jakarta Raya)',
        posted: '5 hours ago'
    },
    {
        title: 'Nelayan Manusia (HeadHunter)',
        company: 'PT MGM Casino',
        description: 'Mengelola seluruh siklus pengembangan produk / aplikasi, mulai dari konsep desain dan perancangan, pengujian, perbaikan guna memastikan kelancaran ',
        salary: 'Rp. Not Specified',
        location: 'Jakarta Barat (Jakarta Raya)',
        posted: '5 hours ago'
    }
];

export class MyCarousel extends Component {

    _renderItem ({item, index}, parallaxProps) {
        return (
            <View style={styles.slide}>
                <Text style={styles.title}>{ item.title }</Text>
                <Text style={styles.title}>{ item.company }</Text>
                <Text style={styles.title}>{ item.description }</Text>
                <Text style={styles.title}>{ item.location }</Text>
                <View style={styles.postedSalarySlide}>
                    <Text style={styles.smallDetails}>{ item.posted }</Text>
                    <Text style={styles.smallDetails}>{ item.salary }</Text>
                </View>
            </View>
        );
    }

    render () {
       
        return (
            <Carousel
                ref={(c) => { this._carousel = c; }}
                data={data}
                renderItem={this._renderItem}
                sliderWidth={sliderWidth}
                itemWidth={itemWidth}
                containerCustomStyle={styles.carouselContainer}
                Carousel layout={'tinder'} 
            />
        );
    }
}

export default class Home extends Component {

    static navigationOptions = {
        title: 'Home',
        headerStyle: {
            backgroundColor: '#090e25',
          },
          headerTintColor: '#fff',
    };

    render() {

        const { navigation } = this.props;

    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.container} onPress={() => navigation.navigate('CardDetail')}>
                <MyCarousel/>
            </TouchableOpacity>
        </View>
    );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'flex-start',
        backgroundColor: '#F5FCFF',
    },
    carouselContainer: {
        flexGrow: 0,
        marginTop: 20
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    smallDetails:{
        fontSize: 15,
        margin: 10,
    },
    slide: {
        width: itemWidth,
        height: itemHeight,
        paddingHorizontal: horizontalMargin,
        //flex: 1,
        backgroundColor : '#d0d8db',
        borderRadius: 10
        // other styles for the item container
    },
    slideInnerContainer: {
        width: slideWidth,
        flex: 1
        // other styles for the inner container
    },
    slideImage: {
        width: '100%',
    },
    postedSalarySlide: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    }
});