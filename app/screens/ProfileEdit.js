import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import { Avatar, Input, Icon } from 'react-native-elements';

export default class ProfileEdit extends Component {
  
  static navigationOptions = {
      title: 'Profile Edit',
      headerStyle: {
        backgroundColor: '#090e25',
      },
      headerTintColor: '#fff',
  };

  constructor(props) {
    super(props);
    this.state = {
        username: "John Doe",
        Location: "Depok",
        Income: "Rp.8.000.000"
    }
  }


  render() {

    return (
      <View style={styles.container}>
        <ScrollView>
            <View>
                <Text style={styles.description}>Username</Text>
                <Input
                placeholder='Username'
                defaultValue={this.state.username}
                />
                <Text style={styles.description}>Location</Text>
                <Input
                placeholder='Location'
                defaultValue={this.state.Location}
                />
                <Text style={styles.description}>Desired Income</Text>
                <Input
                placeholder='Desired Income'
                defaultValue={this.state.Income}
                />
                <TouchableOpacity style={styles.buttonContainer}  onPress={() => this.props.navigation.navigate('Profile')}>
                <Text>Save Edit</Text>  
                </TouchableOpacity>    
            </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header:{
    backgroundColor: "#00BFFF",
    height:100,
  },
  avatarContainer:{
    alignItems: 'center',
    marginTop: 20
  },
  name:{
    fontSize:22,
    color:"#FFFFFF",
    fontWeight:'600',
  },
  body:{
    marginTop:10,
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    padding:30,
  },
  name:{
    fontSize:28,
    color: "#696969",
    fontWeight: "600"
  },
  info:{
    fontSize:16,
    color: "#00BFFF",
    marginTop:10
  },
  description:{
    fontSize:16,
    color: "#696969",
    marginTop:5,
    marginLeft: 10,
    textAlign: 'left'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
    width:340,
    borderRadius:30,
    backgroundColor: "#00BFFF",
  },
});