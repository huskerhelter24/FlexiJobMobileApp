import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { Avatar, Card, ListItem, Button, Icon } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';

export class EducationCard extends Component {

  render() {
    return(
      <View>
        <Card
          title={this.props.level}
          containerStyle={styles.educationCardStyle}>
          <Text style={{marginBottom: 10}}>
            {this.props.name} {this.props.year}
          </Text>
        </Card>
      </View>
    )
  }
}

export class WorkExperienceCard extends Component {

  render(){
    return(
      <View>
        <Card
          containerStyle={styles.educationCardStyle}>
          <Text style={{marginBottom: 10, fontWeight: "600"}}>
            {this.props.position} - {this.props.company}
          </Text>
          <Text>
            {this.props.date}
          </Text>
        </Card>
      </View>
    )
  }
}
export default class Profile extends Component {

  
  static navigationOptions = {
      title: 'Profile',
      headerStyle: {
        backgroundColor: '#090e25',
      },
      headerTintColor: '#fff',
  };

  render() {

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.avatarContainer}>
              <Avatar 
                rounded 
                title="JD"
                size="xlarge"
              />
          </View>
          <View style={styles.body}>
            <View style={styles.bodyContent}>
              <Text style={styles.name}>John Doe</Text>
              <Text style={styles.info}>Depok (Indonesia)</Text>
              <Text style={styles.description}>John.Doe@JD.com</Text>
              <Text style={styles.sectionTitle}>Education</Text>
              <EducationCard level="High School" name="Santa Laurensia" year="(2012-2015)"/>
              <EducationCard level="Bachelor Degree" name="Swiss German University" year="(2015-2019)"/>
              <Text style={styles.sectionTitle}>Work Experience</Text>
              <WorkExperienceCard position="Java Developer" company="Eleh GmbH" date="February 2016 - February 2018"/>
              <WorkExperienceCard position="Permen Developer" company="Tongseng GmbH" date="February 2018 - now"/>
              <TouchableOpacity style={styles.buttonContainer} onPress={() => this.props.navigation.navigate('ProfileEdit')}>
                <Text>Edit Profile</Text>  
              </TouchableOpacity>   
            </View>
          </View>
        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  header:{
    backgroundColor: "#00BFFF",
    height:100,
  },
  educationCardStyle:{
    flexDirection: 'row',
    width: Dimensions.get('window').width - 20,
    justifyContent: 'center'
  },
  sectionTitle:{
    fontSize: 20,
    color: "#696969",
    fontWeight: "600",
    marginTop:10
  },
  avatarContainer:{
    alignItems: 'center',
    marginTop: 20
  },
  body:{
    marginTop:10,
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    padding:10,
  },
  name:{
    fontSize:28,
    color: "#696969",
    fontWeight: "600"
  },
  info:{
    fontSize:16,
    color: "#00BFFF",
    marginTop:10
  },
  description:{
    fontSize:16,
    color: "#696969",
    marginTop:10,
    textAlign: 'center'
  },
  buttonContainer: {
    marginTop:10,
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
    backgroundColor: "#00BFFF",
  },
});