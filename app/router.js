import React, {Component} from 'react';
import {Dimensions, Platform} from 'react-native';
import {StackNavigator, createStackNavigator, createBottomTabNavigator, withNavigation, createAppContainer} from 'react-navigation';
import {Icon} from 'react-native-elements';

import Home from './screens/Home';
import Search from './screens/Search';
import Settings from './screens/Settings';
import Profile from './screens/Profile';
import CardDetail from './screens/CardDetail';
import ProfileEdit from './screens/ProfileEdit';
import Loading from './screens/LoginFlow/Loading';
import SignUp from './screens/LoginFlow/SignUp';
import Login from './screens/LoginFlow/Login';

let screen = Dimensions.get('window');

export const Tabs = createBottomTabNavigator({
    'Home': {
        screen: createStackNavigator({
            Home: Home,
            CardDetail: CardDetail
        }),
        navigationOptions: {
            tabBarLabel: 'Home',
            tabBarIcon: ({tintColor}) => <Icon name="ios-home" type="ionicon" size={28} color={tintColor}/>
        },
    },
    'Search': {
        screen: Search,
        navigationOptions: {
            tabBarLabel: 'Search',
            tabBarIcon: ({tintColor}) => <Icon name="ios-search" type="ionicon" size={28} color={tintColor}/>
        },
    },
    'Settings': {
        screen: createStackNavigator({
            Settings: Settings,
            Profile: Profile,
            ProfileEdit: ProfileEdit
        }),
        navigationOptions: {
            tabBarLabel: 'Settings',
            tabBarIcon: ({tintColor}) => <Icon name="ios-settings" type="ionicon" size={28} color={tintColor}/>
        }
    },
   
});

export const AppRoute = createStackNavigator({
    loginFlow: { 
      screen: createStackNavigator({
        Loading: Loading,
        Login: Login,
        SignUp: SignUp
      },
      { headerMode: 'none' })
    },
    mainFlow: {
      screen: createBottomTabNavigator({
        'Home': {
            screen: createStackNavigator({
                Home: Home,
                CardDetail: CardDetail
            }),
            navigationOptions: {
                tabBarLabel: 'Home',
                tabBarIcon: ({tintColor}) => <Icon name="ios-home" type="ionicon" size={28} color={tintColor}/>
            },
        },
        'Search': {
            screen: Search,
            navigationOptions: {
                tabBarLabel: 'Search',
                tabBarIcon: ({tintColor}) => <Icon name="ios-search" type="ionicon" size={28} color={tintColor}/>
            },
        },
        'Settings': {
            screen: createStackNavigator({
                Settings: Settings,
                Profile: Profile,
                ProfileEdit: ProfileEdit
            }),
            navigationOptions: {
                tabBarLabel: 'Settings',
                tabBarIcon: ({tintColor}) => <Icon name="ios-settings" type="ionicon" size={28} color={tintColor}/>
            }
        }
      }) 
    }
  },{ headerMode: 'none' });

export const createRootNavigator = () => {
    return createAppContainer(AppRoute);
};